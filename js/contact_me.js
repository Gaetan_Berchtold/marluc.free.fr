$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // annulation du comportement par defaut d'envoi du form
            // recup des valeurs du FORM
            var name = $("input#name").val();
            var email = $("input#email").val();
            var message = $("#message-text").val();
            var recaptcha = $("#recaptchaResponse").val();
            var firstName = name; // For Success/Failure Message
            
            // si nom composé (nom + nom de famille séparé par un espace alors on recup le premier nom pour l'utiliser apres)
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            
            $.ajax({
                url: "./mail/contact_me.php",
                type: "POST",
                data: {
                    name: name,
                    email: email,
                    message: message,
                    recaptcha: recaptcha
                },
                cache: false,
                success: function() {
                    // Success + utilisation du prénom séparé précédement
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Merci " + firstName +" votre message à bien été envoyé.</strong>");
                    $('#success > .alert-success')
                        .append('</div>');

                    //reset des champs
                    $('#contactForm').trigger("reset");
                },
                error: function() {
                    // messaage erreur
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-danger').append("<strong>Toutes nos excuses " + firstName + ", il y a eu un probleme choisissez un autre moyen de nous contacter");
                    $('#success > .alert-danger').append('</div>');

                    //reset des champs
                    $('#contactForm').trigger("reset");
                },
            })
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*pour la gestion de l'affichage de resussite ou d'echec */
$('#name').focus(function() {
    $('#success').html('');
});
