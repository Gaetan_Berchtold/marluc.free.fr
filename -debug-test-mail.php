<?php

/** Nouvelle fonction mail pour le FAI Free, conforme au standard
* De temps en temps les courriels ne sont pas envoyés, mais pourtant la fonction mail() renvoie True
* ce qui n'est pas conforme a la spécification PHP de cette fonction.
* De manière empirique, il a été déterminée qu'un temps d'envoi au moins égal à 2 secondes est une garantie que le courriel
* est vraiment envoyé.
* Si le mail est vraiment envoyé, une notification de rejet est bien envoyé par Free à l'adresse de l'expéditeur du message
* Copyright 2013 - a@a.a <tmp12311@free.fr>
* Licence : CeCILL-B, http://www.cecill.info
* Merci à Gaming Zone <http://gaming.zone.online.fr> pour ses tests ayant permis de déterminer la durée
* */
function mailFree($to , $subject , $message , $additional_headers=null , $additional_parameters=null) {
$start_time = time();
$resultat=mail ( $to , $subject, $message, $additional_headers, $additional_parameters);
$time= time()-$start_time;
return $resultat & ($time>1);
}
/** Fin de la définition de la fonction*/

/** Le code qui suit est juste donné comme exemple de test de la fonction
*
* Code de test de la fonction
* Modifié par Al <les.pages.perso.chez(chez)free.fr>
*  */

/* Mettre ici l'adresse mail de votre site Web : si votre site est http://monsite.free.fr/ alors votre adresse email est monsite@free.fr */
$admin = 'gberchtold@free.fr';

$debut = 	'<!DOCTYPE html>
			<html lang="fr">
			<head>
			<meta charset="utf-8" />
			<title>Test de la fonction PHP mail() chez Free - Test d\'envoi</title>
			<meta name="robots" content="noindex,nofollow,noarchive"/>
			<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
			</head><body>
			<h1>Test de la fonction PHP mail() chez Free - Test d\'envoi</h1>
			<h2>Appel à la fonction mail() réalisé</h2>
			<hr/>
			<h3>Erreur(s) détectée(s): </h3>';

$out=' ';
$res=false;

if (empty($_POST)||isset($_POST['reset'])){
	$dest='';
} else {
	$dest=$_POST['dest'];
	if (isset($_POST['send'])){
		$sujet='Message envoyé le '.date("l j F Y").' à '.date("H:i:s");
		$message="Ma foi,\nTout semble fonctionner correctement.\n\nEnvoyé depuis l'IP={$_SERVER["REMOTE_ADDR"]}";
		$additional_headers = "Cc: $admin\r\n";
		$additional_headers .= "From: $admin\r\n";
		$additional_headers .= "MIME-Version: 1.0\r\n";
		$additional_headers .= "Content-Type: text/plain; charset=utf-8\r\n";
		// $additional_headers .="Reply-To: $admin\r\n";
		$additional_headers .="Return-Path: $admin\r\n";
			if (mailFree( $dest, $sujet , $message, $additional_headers )==false) {
				$out.="<pre style='border: 1px dotted #666666;padding:10px;'><code>L'envoi du message n'a pas été réalisé en raison des limitations des serveurs de Free, merci de réessayer un peu plus tard.</code></pre>";
			} else {
				$res=true;
			}
	}
}

if (!$res) {
$out.="<form id='contact' method='post'>
<p><strong>Tous les champs sont obligatoires.</strong></p>
<p><label for='dest'>Courriel pour la réponse :</label> <input type='text' name='dest' id='dest' value='$dest'/></p>
<p><input type='reset' name='reset' value='Effacer' /> <input type='submit' name='send' value='Envoyer'/></p>
</form>";
} else {
	$out.="<pre style='border: 1px dotted #666666;padding:10px;'><code>Merci de votre visite, vous allez recevoir un message à l'adresse : $dest</code></pre>";
}

$fin='<p>Si une erreur du serveur ou un warning du type <code><strong>Warning:</strong> mail() [function.mail]: Trop de spam. Fonction mail() bloquée. in <strong>/mnt/000/xxx/x/0/votrelogin/test-mail.php</strong> on line <strong>17</strong></code> apparaît dans la zone de notification, la fonction PHP <code>mail()</code> de votre compte est bloquée pour spam ou usage excessif. Selon les cas, la fonction PHP <code>mail()</code> sera débloquée automatiquement toutes les fins de semaines ou de temps en temps.</p>
<p>Si il n\'apparaît dans la zone de notification aucun warning ou message d\'erreur, la fonction PHP <code>mail()</code> de votre compte fonctionne correctement.</p>
<p>Des informations additionnelles sont disponibles dans le billet <a href="http://les.pages.perso.chez.free.fr/l-art-d-envoyer-des-mails-depuis-les-pp-de-free" hreflang="fr" title="L\'art et la manière d\'envoyer des mails depuis les pages perso de Free…">L\'art et la manière d\'envoyer des mails depuis les pages perso de Free…</a> du site <em>Les Pages Perso Chez Free</em>.</p></body></html>';

echo $debut.$out.$fin;
ob_end_flush();
?>