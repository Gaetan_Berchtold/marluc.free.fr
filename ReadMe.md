# Site vitrine en "dur" pour **Marluc.free.fr**

- Pas de back-office car le proprio veut pouvoir "toucher du code".
- Le dossier OLD contiendra l'ancien site.

Le **ReCaptcha** est une **version V3** à modifier avec une nouvelle **clef publique** et **privée** à chaque changement de nom de domaine les clefs contenues dans ce repo sont donc inutilisables...

- Le script Recaptcha de **Google** est ligne 17 du `index.html`.

La response du Captcha doit être envoyée via le formulaire (dans la balise <form>) grace à: 
```html
<input type="hidden" name="recaptcha_response" id="recaptchaResponse">
```

Le formulaire est ensuite envoyé en AJAX grace au script `contact_me.js` , il faut donc penser à y joindre aussi la réponse du captcha !
```javascript
var recaptcha = $("#recaptchaResponse").val();
```

```javascript
$.ajax({
    url: "/contact_me.php",
    type: "POST",
    data: {
        name: name,
        email: email,
        message: message,
        recaptcha: recaptcha  
    },
    ...
```

Il est envoyé au `contact_me.php` et récupéré par la superglobale `$_POST['recaptcha']` puis json_décodé.
Si le score est sup a 0.5 (réglage par defaut de Google) alors on estime que c'est un humain sinon c'est un bot !

## Fichiers -debug-xxx.php
- Une verif du serveur grace au script `-debug-phpinfo.php` (inclus) permet (en plus d'afficher toutes les informations du serveur) de savoir si le serveur SMTP est bien configuré pour l'envoi de **Mails** au sein d'apache cf: https://php.developpez.com/faq/?page=mail
- Suite à de trop nombreux tests durant la phase de "dev", le serveur (ou l'hebergeur) peut peu se mettre en securité et bloquer les envois de mails, le script `-debug-test-mail.php` sert justement à tester si le serveur est en carafe.
