<?php  
    	
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha'])) {
   echo"retour du captcha OK   \n";
   // Build POST request:
   $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
   $recaptcha_secret = '6LeTZscUAAAAAHKizeSCqB14E_HlY-XaJ3V0Lpvr';
   $recaptcha_response = $_POST['recaptcha'];

   // decode POST:
   $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
   $recaptcha = json_decode($recaptcha);

  
   // suivant le score du captcha retourné
   // si le scrore est sup à 0.5
   if ($recaptcha->score >= 0.5) {      
      echo "score Captcha OK (humain detecté) \n";
      // Verif si des champs sont vides
      // alors retourne false et var dump les erreurs pour debug
      if(empty($_POST['name'])  		||
         empty($_POST['email']) 		||
         empty($_POST['message'])	||
         !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
         {
            echo "une des variables est vide !    ";
            //pour le debug
            var_dump(
               "name: ". $_POST['name']. " ".
               "email: ". $_POST['email']. " ".
               "message: ". $_POST['message']
            );
            return false;
      
      // si pas vide alors envoi du mail
      }else{
         //pour verif
         /* var_dump(
            "name: ".$_POST['name']." ".
            "email: ".$_POST['email']." ".
            "message: ".$_POST['message']
         ); */

         $name = $_POST['name'];
         $email_address = $_POST['email'];
         $message = $_POST['message'];

         // Création de l'email
         $to = "marluc@free.fr";          
         $email_subject = "RC-Usaf - Marluc.free.fr - Messagerie Interne";
         $email_body = "<h1>Yop Marco t'as recu un message de la part de <b>$name</b>.<br /><br /></h1>" . 
                        "<h2>Envoyé le ". date('l j F Y') . " à " . date('H:i:s') . "</h2><br /><br />" .
                        "<b>Nom:</b> $name <br />
                        <b>Email:</b> $email_address<br />
                        <b><u>Message:</u></b> $message </p><br /><br />" .
                        "Et en plus,<br />Tout semble fonctionner correctement! Plutot cool non ?<br /><br />IP de l'expediteur: {$_SERVER["REMOTE_ADDR"]}  ;)";
         $headers = "From: marluc@free.fr\n";
         //$headers .= "Cc: sethgrecko@gmail.com\n";
         $headers .= "Cc: marcbahuaud@gmail.com\n";         
         $headers .= "Reply-To: $email_address\n";
         $headers .= 'MIME-Version: 1.0' . "\r\n";
         $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
         
         //envoi du mail
         $send = mail($to,$email_subject,$email_body,$headers);

         //si le SEND OK
         if ($send){
            echo "success mail envoyé";
            return true;
         
         //sinon
         }else{ 
            echo "invalide mail non envoyé";
            return false;
         }

      }
   }
   // si score captcha inf a 0.5
	else {
      echo"score < à 0.5 = synonyme de BOT \n";
      // Creation du mail d'avertissement
      $to = "marluc@free.fr"; 
      $email_subject = "RC-Usaf - Marluc.free.fr - Attention Bot detecté";
      $email_body = "<h1>Yop Marco Y'a un probleme sur ton site le captcha Google a détecté une activité anormale </h1>" . 
                     "<h2>Envoyé le ". date('l j F Y') . " à " . date('H:i:s') . "</h2><br /><br />";
      $headers = "From: marluc@free.fr\n";
      //$headers .= "Cc: sethgrecko@gmail.com\n";
      $headers .= "Cc: marcbahuaud@gmail.com\n";        
      $headers .= 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
      
   
      $send = mail($to,$email_subject,$email_body,$headers);//envoi du mail
   }
   
}else{
    echo"Aucun retour du captcha";
   /*var_dump("MON server: ".$_SERVER['REQUEST_METHOD']);
   var_dump("MON post recaptcha: " . $_POST['recaptcha']); */
}